package ece651.sp22.jz399.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.function.Function;

public class Actions {
    private int fireRemains;
    private int sonarRemains;
    private int moveRemains;
    private final PrintStream out;
    private final BufferedReader inputReader;
    private final HashMap<Character, Double> orientationTodegree;

    public Actions(int sonarRemains, int moveRemains, PrintStream out, BufferedReader inputReader) {
        this.fireRemains = Integer.MAX_VALUE;
        this.sonarRemains = sonarRemains;
        this.moveRemains = moveRemains;
        this.out = out;
        this.inputReader = inputReader;
        this.orientationTodegree = new HashMap<Character, Double>();
        create_OriToDegreeMapping();
    }

    private void create_OriToDegreeMapping() {
        double PAI = 3.14159;
        orientationTodegree.put('H', 0 * PAI);
        orientationTodegree.put('V', -1 * PAI / 2);
        orientationTodegree.put('U', PAI / 2);
        orientationTodegree.put('D', -1 * PAI / 2);
        orientationTodegree.put('L', 2 * PAI / 2);
        orientationTodegree.put('R', 0 * PAI / 2);
    }

    public String selectAction(String name) throws IOException {
        String instruction = "--------------------------------------------------------------------------------\n" +
                "Possible actions for Player " + name + ": \n" + "\n" +
                "F Fire at a square\n" +
                "M Move a ship to another square (" + Integer.toString(moveRemains) + " remaining)\n" +
                "S Sonar scan (" + Integer.toString(sonarRemains) + " remaining)\n" +
                "Player " + name + ", what would you like to do?\n" +
                "---------------------------------------------------------------------------\n";

        while (true) {
            out.print(instruction);
            String s = inputReader.readLine();
            if (s.equals("F") && fireRemains > 0) {
                fireRemains--;
                return s;
            } else if (s.equals("M") && moveRemains > 0) {
                moveRemains--;
                return s;
            } else if (s.equals("S") && sonarRemains > 0) {
                sonarRemains--;
                return s;
            } else {
                out.println("Wrong action type\n");
            }
        }
    }

    public void fireAt(Board<Character> enemyBoard) throws IOException {
        // prompt to fire
        Coordinate c = null;
        String instruction = "Enter a coordinate you want fire at:\n";
        while (true) {
            try {
                c = (Coordinate) inputPosition(instruction, "Coordinate");
                if (c.getRow() >= enemyBoard.getHeight() || c.getColumn() >= enemyBoard.getWidth()) { // 保证坐标在地图内
                    throw new IllegalArgumentException("coordinate is out of the board.\n");
                }
                break;
            } catch (Exception e) {
                out.println(e.getMessage());
            }
        }

        // fire and report result
        Ship<Character> hitShip = enemyBoard.fireAt(c);
        if (hitShip == null)
            out.println("You missed!");
        else {
            out.println("You hit a " + hitShip.getName() + "!");
        }
    }

    public void sonarScan(Board<Character> enemyBoard) {
        int d = 3; // 确定扫描范围半轴长
        Coordinate center = null;

        // 获取扫描区域的中心坐标
        String instruction = "Please enter the center coordinate for the scan region:\n";
        while (true) {
            try {
                center = (Coordinate) inputPosition(instruction, "Coordinate");
                if (center.getRow() >= enemyBoard.getHeight() || center.getColumn() >= enemyBoard.getWidth()) { // 保证坐标在地图内
                    throw new IllegalArgumentException("coordinate is out of the board.\n");
                }
                break;
            } catch (Exception e) {
                out.println(e.getMessage());
            }
        }

        // 扫描指定区域
        int countSubmarines = 0;
        int countDestroyers = 0;
        int countBattleships = 0;
        int countCarriers = 0;
        for (int i = -1 * d; i <= d; i++) {
            for (int j = -1 * d; j <= d; j++) {
                if (Math.abs(i) + Math.abs(j) > d) // 忽略扫描区域外的点
                    continue;
                if (i + center.getRow() >= enemyBoard.getHeight() || j + center.getColumn() >= enemyBoard.getWidth()) // 忽略超出board范围的点
                    continue;

                Coordinate checkCoord = new Coordinate(i + center.getRow(), j + center.getColumn());
                Character res = enemyBoard.whatIsAtForSelf(checkCoord);
                if(res == null)
                    continue;
                else if (res == 'c')
                    countCarriers++;
                else if (res == 'd')
                    countDestroyers++;
                else if (res == 's')
                    countSubmarines++;
                else if (res == 'b')
                    countBattleships++;
            }
        }

        // 显示扫描结果
        out.println("---------------------------------------------------------------------------\n");
        out.println("Submarines occupy "+ countSubmarines + " squares");
        out.println("Destroyers occupy "+ countDestroyers + " squares");
        out.println("Battleships occupy "+ countBattleships + " squares");
        out.println("Carriers occupy "+ countCarriers + " squares");
        out.println("---------------------------------------------------------------------------\n");

    }

    public void moveShip(Board<Character> myBoard,
            HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns) {

        // 选择一艘自己的船，准备将其移动，将其移出myShips列表,并加入oldShips列表
        Ship<Character> chosenOldShip = chooseShip(myBoard);
        // out.println("chooseShip.name: "+ chosenOldShip.getName());
        myBoard.deleteNewShip(chosenOldShip);
        myBoard.recordOldShip(chosenOldShip);

        // 输入坐标在新位置建一艘船
        String instruction = "Please enter a new placement for the ship you want to move: \n ";
        Ship<Character> newShip = null;
        while (true) {
            try {
                // 获取正确格式的新坐标
                Placement p = (Placement) inputPosition(instruction, "Placement");

                // 根据新坐标构建新船,并将其加入myShips列表中
                newShip = shipCreationFns.get(chosenOldShip.getName()).apply(p);
                String situtation = myBoard.tryAddShip(newShip);
                if (situtation != null) // 新船与其他船只冲突或越界
                    throw new IllegalArgumentException(situtation);
                break;
            } catch (Exception e) {
                out.println(e.getMessage());
            }
        }

        // 新旧船damage迁移
        Iterable<Coordinate> coordList = chosenOldShip.getCoordinates();
        for (Coordinate c : coordList) {
            if (chosenOldShip.wasHitAt(c) == false) // 排除Ships上完好的点
                continue;

            // 获取相对直角坐标
            Coordinate oldCenterCoord = chosenOldShip.getCenterCoord();
            // out.println("oldCenterCoord.row: "+ oldCenterCoord.getRow() + "
            // oldCenterCoord.col:" + oldCenterCoord.getColumn());
            double deltaRow = c.getRow() - oldCenterCoord.getRow();
            double deltaCol = c.getColumn() - oldCenterCoord.getColumn();
            // out.println("deltaRow: "+ deltaRow + " deltaCol:" + deltaCol);

            // 转换为相对中心点的极坐标
            double r = Math.sqrt(Math.pow(deltaRow, 2) + Math.pow(deltaCol, 2));
            double sita = Math.atan2(deltaRow, deltaCol); // 范围(-pai ~ pai)
            // out.println("r: "+ r + " sita:" + sita);

            // 通过新旧方向计算出旋转角度
            Character oldOrientation = chosenOldShip.getOrientation();
            Character newOrientation = newShip.getOrientation();
            double deltaSita = orientationTodegree.get(newOrientation) - orientationTodegree.get(oldOrientation);
            // out.println("deltaSita: "+ deltaSita);

            // 计算旋转后新极坐标
            double new_r = r;
            double new_sita = sita - deltaSita; // 反方向旋回去
            // out.println("new_r: "+ new_r + " new_sita:" + new_sita);

            // 新极坐标转为直角坐标系，并加上新的中心坐标作为偏移量
            Coordinate newCenterCoord = newShip.getCenterCoord();
            // out.println("newCenterCoord.row: "+ newCenterCoord.getRow() + "
            // newCenterCoord.col:" + newCenterCoord.getColumn());
            int newRow = (int) (newCenterCoord.getRow() + Math.round(new_r * Math.sin(new_sita)));
            int nweCol = (int) (newCenterCoord.getColumn() + Math.round(new_r * Math.cos(new_sita)));
            // out.println("newRow: "+ newRow + " nweCol:" + nweCol);

            newShip.recordHitAt(new Coordinate(newRow, nweCol));
        }

    }

    // 在moveShip动作中，提示用户输入坐标，确定要移动的船，返回该船的object
    private Ship<Character> chooseShip(Board<Character> myBoard) {

        // 用户输入坐标,检查坐标格式以及是否正确对应船只
        String instruction = "Enter the Coordinate of the ship you want to move:\n";
        while (true) {
            try {
                // 获取正确格式的坐标
                Coordinate c = (Coordinate) inputPosition(instruction, "Coordinate");

                // 根据坐标搜索对应船只
                Ship<Character> ship = myBoard.searchShip(c);
                if (ship == null)
                    throw new IOException("This coordinate does to match any ships.\n");
                else
                    return ship;
            } catch (Exception e) {
                out.print(e.getMessage());
            }

        }

    }

    // 将输入字符串转为对应的坐标，保证坐标格式的正确
    private Object inputPosition(String instruction, String type) throws IOException {
        Object position = null;
        out.print(instruction);
        String s = inputReader.readLine();
        if (s == null)
            throw new EOFException("does not enter anything, cause EOF excepetion");
        if (type.equals("Coordinate")) {
            position = new Coordinate(s);
        } else if (type.equals("Placement")) {
            position = new Placement(s);
        } else {
            throw new IllegalArgumentException("wrong type position you want to generate.\n");
        }

        return position;

    }

}