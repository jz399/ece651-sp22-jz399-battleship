package ece651.sp22.jz399.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */

public class BoardTextView {
    private final Board<Character> toDisplay; // 存储board上带有的ship信息，根据这些信息进行打印显示

    /**
     * Constructs a BoardView, given the board it will display.
     * 
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getHeight() > 26 || toDisplay.getWidth() > 10) {
            throw new IllegalArgumentException("Board must be no larger than 10x26\n");
        }
    }

    /**
     * This function should be call by player to display his own board
     * 
     * @return the String that is the board for the given board
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    /**
     * This function should be call by player to diplay his enemy's board
     * 
     * @return the String that is the board for the given board
     */
    public String displayEnemyBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    private String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String res = "";
        String header = makeHeader();
        res += header;

        char index = 'A';
        for (int i = 0; i < toDisplay.getHeight(); i++) {
            String line = "";

            // add col 0 element
            Character c = getSquareFn.apply(new Coordinate(i, 0));
            if (c == null)
                line = line + index + ' ';
            else
                line = line + index + c;

            // add remain
            for (int j = 1; j < toDisplay.getWidth(); j++) {
                c = getSquareFn.apply(new Coordinate(i, j));
                if (c == null)
                    line = line + '|' + ' ';
                else
                    line = line + '|' + c;
            }

            line = line + index + '\n';
            res = res + line;
            index += 1;
        }
        return res + header;
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep = ""; // start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append('\n');
        return ans.toString();
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        StringBuilder sb = new StringBuilder();
        assert (toDisplay.getWidth() == enemyView.toDisplay.getWidth());
        assert (toDisplay.getHeight() == enemyView.toDisplay.getHeight());
        int w = toDisplay.getWidth();
        int h = toDisplay.getHeight();

        // make header
        for (int i = 0; i < 5; i++) {
            sb.append(" ");
        }
        sb.append(myHeader);
        for (int i = 5 + myHeader.length(); i < 3 * w + 25; i++) {
            sb.append(" ");
        }
        sb.append(enemyHeader);
        sb.append("\n");

        // make board
        String[] my_lines = displayMyOwnBoard().split("\n");
        String[] enemy_lines = enemyView.displayEnemyBoard().split("\n");
        for (int i = 0; i < h; i++) {
            sb.append(my_lines[i]);
            for (int j = w; j < 2 * w + 19; j++) {
                sb.append(" ");
            }
            sb.append(enemy_lines[i]);
            sb.append("\n");
        }

        return sb.toString();
    }

}