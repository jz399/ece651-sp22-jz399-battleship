package ece651.sp22.jz399.battleship;

public abstract class PlacementRuleChecker<T> {
  private final PlacementRuleChecker<T> next; // link to the next checker

  public PlacementRuleChecker(PlacementRuleChecker<T> next) {
    this.next = next;
  }

  // use to implements details chekc rules.
  protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

  // subclass should not override this function
  public String checkPlacement(Ship<T> theShip, Board<T> theBoard) { // 链式组合条件判断
    // if we fail our own rule: stop the placement and return the explaination
    String situtaion = checkMyRule(theShip, theBoard);
    if (situtaion != null) {
      return situtaion;
    }
    // other wise, ask the rest of the chain.
    if (next != null) {
      return next.checkPlacement(theShip, theBoard);
    }
    // if there are no more rules, then the placement is legal
    return null;
  }
}