package ece651.sp22.jz399.battleship;

import java.io.*;
import java.util.function.Function;

public class TextPlayer extends Player {
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
  
    // constructor
    public TextPlayer(Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
            AbstractShipFactory<Character> shipFactory, String name, Actions Actions) {
        super(theBoard, shipFactory, name, Actions);
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
    }

    // 根据输入坐标，返回一个placement
    protected Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        if (s == null)
            throw new EOFException("does not enter placement, cause EOF excepetion.\n");
        return new Placement(s);
    }

    // 根据输入的坐标，生成BasicShip并加入地图中，输出地图
    private boolean doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        
        Placement p = null;
        Ship<Character> s = null;
        try {  // handle exceptions caused by wrong format Placement
            p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
            s = createFn.apply(p); // 生成船只时可能因为方向不匹配，产生异常
        } catch (Exception e) {
            out.println(e.getMessage());
            return false;
        }

        String situtaion = theBoard.tryAddShip(s);
        if (situtaion == null) { // add ship successfully
            out.print(view.displayMyOwnBoard());
            out.print("\n");
            return true;
        } else { // fail to add ship
            out.print(situtaion);
            return false;
        }
    }

    // 完成船只放置阶段
    @Override
    public void doPlacementPhase() throws IOException {
        // show an empty board
        out.println(view.displayMyOwnBoard());

        // show instructions
        String instruction = "--------------------------------------------------------------------------------\n" +
                "Player " + name + " : you are going to place the following ships (which are allrectangular).\n" +
                "For each ship, type the coordinate of the upper left side of the ship, followed by either H (for horizontal) or V (for vertical).\n"
                +
                "For example M4H would place a ship horizontally starting at M4 and going to the right.\n" +
                "You have:\n 2 Submarines ships that are 1x2 \n 3 Destroyers that are 1x3\n 3 Battleships that are 1x4\n 2 Carriers that are 1x6.\n"
                +
                "--------------------------------------------------------------------------------";
        out.println(instruction);

        // place ships
        for (int i = 0; i < shipsToPlace.size(); i++) {
            String shipName = shipsToPlace.get(i);
            if (doOnePlacement(shipName, shipCreationFns.get(shipName)) == false) // 假如船加入失败，重新加入该船
                i--;
        }
    }

    // 当前player选择并执行动作
    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        String myHeader = "Your ocean";
        String enemyHeader = "Player " + enemyName + "'s ocean:\n";

        // diplay the board
        out.println("Player " + name + "'s turn:\n");
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, myHeader, enemyHeader));

        // select Action
        String type = Actions.selectAction(name);
        if (type.equals("M"))
            Actions.moveShip(theBoard, shipCreationFns);
        else if (type.equals("S")) {
            Actions.sonarScan(enemyBoard);
        } else {
            Actions.fireAt(enemyBoard);
        }

    }

}