package ece651.sp22.jz399.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 'b', "BattleShip");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    // just for test
    public Ship<Character> makeWrongShip_test(Placement where){
        return createShip(where, 'c', "Destroyer");
    }

    // Accept Parameter w,h to create RectangleShip
    private Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        Ship<Character> ship = null;
        ship = new RectangleShip<Character>(name, where.getCoordinate(), where.getOrientation(), w, h, letter, '*');

        return ship;
    }

    // create ships in other shape
    private Ship<Character> createShip(Placement where, char letter, String name) {
        Ship<Character> ship = null;
        if (name == "BattleShip")
            ship = new TriangleShip<Character>(name, where.getCoordinate(), where.getOrientation(), letter, '*');
        else if (name == "Carrier")
            ship = new ParallelogrmShip<Character>(name, where.getCoordinate(), where.getOrientation(), letter, '*');

        return ship;

    }

}