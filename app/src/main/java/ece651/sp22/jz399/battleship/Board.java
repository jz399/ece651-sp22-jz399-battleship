package ece651.sp22.jz399.battleship;


public interface Board<T> {
    /**
     * return the Width of the board
     * 
     * @return the Width of the board
     */
    public int getWidth();

    /**
     * return the Height of the board
     * 
     * @return the Height of the board
     */
    public int getHeight();

    /**
     * return what is at Coordinate where in the Board in self perspective.
     * 
     * @param where the Coordinate we need to search
     * @return what the board will display in this coordinate
     */
    public T whatIsAtForSelf(Coordinate where);

    /**
     * return what is at Coordinate where in the Board in enemy perspective.
     * 
     * @param where the Coordinate we need to search
     * @return what the board will display in this coordinate
     */
    public T whatIsAtForEnemy(Coordinate where);

    /**
     * try to add a ship to the board
     * 
     * @param toAdd the ship we need to add
     * @return null if add successfully,otherwise return the string points out
     *         failure reasons.
     */
    public String tryAddShip(Ship<T> toAdd);

    /**
     * add an old ship to the oldShips list
     * 
     * @param oldShip the ship we need to add
     * @return nothing return 
     */
    public void recordOldShip(Ship<T> oldShip);

    /**
     * fire at the Coordinate. required to pass valid Coordinate. Function allows
     * to fire at the same coordinate many times.
     * @param c the Coordinate we want to fire at.
     * @return null if we miss,otherwise return the ship we fire at.
     */
    public Ship<T> fireAt(Coordinate c);

    /**
     * search for ships in the board based on Coordinate
     * 
     * @param c the Coordinate we use to search for ship.
     * @return null that ship does not exist, or return the result
     */
    public Ship<T> searchShip(Coordinate c);

    /**
     * delete the ship in myShip list
     * 
     * @param c the Coordinate we use to search for ship.
     * @return null that ship does not exist, or return the result
     */
    public void deleteNewShip(Ship<T> ship);

    /**
     * check whether all the ships are sunk. Use to check for lose.
     * 
     * @return true if all the ships in myships are sunk ,other return false
     */
    public boolean checkWetherAllSunk();

}