package ece651.sp22.jz399.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> set = theShip.getCoordinates();
        for (Coordinate c : set) {
            int row = c.getRow();
            int col = c.getColumn();
            if (row < 0 || row >= theBoard.getHeight()) // 坐标范围[0,width-1]/[0,height-1]
                return "That placement is invalid: the ship goes off the board.\n";
            if (col < 0 || col >= theBoard.getWidth())
                return "That placement is invalid: the ship goes off the board.\n";
        }
        return null;
    }

}