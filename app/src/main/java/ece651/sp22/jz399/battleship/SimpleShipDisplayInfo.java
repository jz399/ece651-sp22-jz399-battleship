package ece651.sp22.jz399.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    private T myData;
    private T onHit;

    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    @Override
    // where->为后续拓展留下接口， 可能同一艘船不同位置，显示内容不一样
    public T getInfo(Coordinate where, boolean hit) {
        if (hit == true)
            return onHit;
        else
            return myData;
    }

}