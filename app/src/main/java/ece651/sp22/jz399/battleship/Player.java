package ece651.sp22.jz399.battleship;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public abstract class Player {
    final Board<Character> theBoard;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace; // store a list of ship names
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns; // a map from ship name to lambda
                                                                                 // function
    final Actions Actions;

    // constructor
    public Player(Board<Character> theBoard, AbstractShipFactory<Character> shipFactory, String name, Actions Actions) {
        this.theBoard = theBoard;
        this.shipFactory = shipFactory;
        this.name = name;
        this.Actions = Actions;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        setupShipCreationList();
        setupShipCreationMap();
    }   

    private void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("BattleShip", (p) -> shipFactory.makeBattleship(p));
    }

    private void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "BattleShip"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public void doPlacementPhase() throws IOException{}

    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException{}
}