package ece651.sp22.jz399.battleship;

import java.io.IOException;
import java.io.PrintStream;

public class ComputerTextPlayer extends Player {
    final PrintStream out;

    // constructor
    public ComputerTextPlayer(Board<Character> theBoard, AbstractShipFactory<Character> shipFactory, String name,
            ece651.sp22.jz399.battleship.Actions Actions, PrintStream out) {
        super(theBoard, shipFactory, name, Actions);
        this.out = out;
    }

    // 完成船只放置阶段
    @Override
    public void doPlacementPhase() throws IOException {
        String shipPlacements[] = { "a0v", "a1v", "a2v", "a3v", "a4v",
                "F0u", "F3u", "f6u", "m3u", "m7u" };

        out.println("Computer Player " + name + " begin to place ships\n");
        for (int i = 0; i < shipsToPlace.size(); i++) {
            String shipName = shipsToPlace.get(i);
            Placement p = new Placement(shipPlacements[i]);
            Ship<Character> ship = shipCreationFns.get(shipName).apply(p);
            String situtaion = theBoard.tryAddShip(ship);
        }
    }

    // 当前player选择并执行动作,Computer只会随机fire.
    @Override
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {

        // 生成随机攻击坐标
        int row = 0 + (int) (Math.random() * (enemyBoard.getHeight() - 0));
        int col = 0 + (int) (Math.random() * (enemyBoard.getWidth() - 0));
        Coordinate fireCoord = new Coordinate(row, col);

        // 开火
        Ship<Character> hitShip = enemyBoard.fireAt(fireCoord);
        if (hitShip == null)
            out.println("Player " + name + " miss!\n");  
        else {
            out.println("Player " + name + " hit your " + hitShip.getName() + " at " + fireCoord.toString());
        }

    }

}