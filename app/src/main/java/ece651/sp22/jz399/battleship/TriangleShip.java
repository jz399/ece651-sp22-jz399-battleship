package ece651.sp22.jz399.battleship;

import java.util.HashSet;

public class TriangleShip<T> extends BasicShip<T> {
    private final String name;
    private final static int[][] upMatrix = {
            { 0, 1, 0 },
            { 1, 2, 1 }
    };
    private final static int[][] downMatrix = {
            { 1, 2, 1 },
            { 0, 1, 0 }
    };
    private final static int[][] rightMatrix = {
            { 1, 0 },
            { 2, 1 },
            { 1, 0 }
    };
    private final static int[][] leftMatrix = {
            { 0, 1 },
            { 1, 2 },
            { 0, 1 }
    };

    // generate coordinates for this ship.
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, Character orientation) throws IllegalArgumentException {
        HashSet<Coordinate> set = new HashSet<Coordinate>();
        int row_upperLeft = upperLeft.getRow();
        int col_upperLeft = upperLeft.getColumn();

        if (orientation == 'U') {
            iterMatrix(set, row_upperLeft, col_upperLeft, 2, 3, upMatrix);
        } else if (orientation == 'D') {
            iterMatrix(set, row_upperLeft, col_upperLeft, 2, 3, downMatrix);
        } else if (orientation == 'L') {
            iterMatrix(set, row_upperLeft, col_upperLeft, 3, 2, leftMatrix);
        } else if (orientation == 'R') {
            iterMatrix(set, row_upperLeft, col_upperLeft, 3, 2, rightMatrix);
        } else
            throw new IllegalArgumentException("wrong orientation for TriangleShip.\n");

        return set;
    }

    private static Coordinate iterMatrix(HashSet<Coordinate> set, int row_upperLeft, int col_upperLeft, int mat_h, int mat_w,
            int[][] matrix) {

        Coordinate centerCoord = null;
        for (int i = 0; i < mat_h; i++) {
            for (int j = 0; j < mat_w; j++) {
                if (matrix[i][j] != 0) {
                    set.add(new Coordinate(row_upperLeft + i, col_upperLeft + j));
                }
                if (matrix[i][j] == 2) {
                    centerCoord = new Coordinate(row_upperLeft + i, col_upperLeft + j);
                }
            }
        }
        return centerCoord;
    }

    private Coordinate getCenterCoord(Coordinate upperLeft, Character orientation){
        Coordinate CenterCoord = null;
        HashSet<Coordinate> set = new HashSet<Coordinate>();
        int row_upperLeft = upperLeft.getRow();
        int col_upperLeft = upperLeft.getColumn();

        if (orientation == 'U') {
            CenterCoord = iterMatrix(set, row_upperLeft, col_upperLeft, 2, 3, upMatrix);
        } else if (orientation == 'D') {
            CenterCoord = iterMatrix(set, row_upperLeft, col_upperLeft, 2, 3, downMatrix);
        } else if (orientation == 'L') {
            CenterCoord = iterMatrix(set, row_upperLeft, col_upperLeft, 3, 2, leftMatrix);
        } else if (orientation == 'R') {
            CenterCoord = iterMatrix(set, row_upperLeft, col_upperLeft, 3, 2, rightMatrix);
        } else
            throw new IllegalArgumentException("wrong orientation for ParallelogrmShip.\n");

        return CenterCoord;
    }

    public TriangleShip(String name, Coordinate upperLeft, Character orientation, ShipDisplayInfo<T> myShipDisplayInfo,
            ShipDisplayInfo<T> EnemyShipDisplayInfo) throws IllegalArgumentException {
        super(makeCoords(upperLeft, orientation), myShipDisplayInfo, EnemyShipDisplayInfo);
        this.name = name;
        this.orientation = orientation;
        this.centerCoord = getCenterCoord(upperLeft, orientation);
    }

    public TriangleShip(String name, Coordinate upperLeft, Character orientation, T data, T onHit) {
        this(name, upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    @Override
    public String getName() {
        return name;
    }
}