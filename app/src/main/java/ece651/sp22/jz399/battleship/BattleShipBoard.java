package ece651.sp22.jz399.battleship;

import java.util.ArrayList;
//import ece651.sp22.jz399.battleship.NoCollisionRuleChecker;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final ArrayList<Ship<T>> myShips;
    private final ArrayList<Ship<T>> oldShips;
    private final PlacementRuleChecker<T> placementChecker;
    private HashSet<Coordinate> enemyMisses;
    private final T missInfo;
    private final ArrayList<ArrayList<T>> enemyViewBoard; //记录敌人视角下当前board的样子

    /*
     * Constructs a BattleShipBoard with the specified width
     * and height
     * 
     * @param w is the width of the newly constructed board.
     * 
     * @param h is the height of the newly constructed board.
     * 
     * @throws IllegalArgumentException if the width or height are less than or
     * equal to zero.
     */
    BattleShipBoard(int width, int height, T missInfo) {
        if (width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
        }
        this.height = height;
        this.width = width;
        this.myShips = new ArrayList<Ship<T>>();
        this.oldShips = new ArrayList<Ship<T>>();
        this.placementChecker = new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)); // 后期继续添加规则
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
        this.enemyViewBoard = createEnemyViewBoard(height, width);
    }

    private ArrayList<ArrayList<T>> createEnemyViewBoard(int height, int width) {
        ArrayList<ArrayList<T>> list = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            ArrayList<T> row = new ArrayList<T>();
            for (int j = 0; j < width; j++) {
                row.add(null);
            }
            list.add(row);
        }
        return list;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public String tryAddShip(Ship<T> toAdd) {
        String situtaion = placementChecker.checkPlacement(toAdd, this);
        if (situtaion == null) { // add ship successfully
            myShips.add(toAdd);
            return null;
        } else // fail to add ship
            return situtaion;
    }

    @Override
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    @Override
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    // return what is at in two perspective
    private T whatIsAt(Coordinate where, boolean isSelf) {
        if (isSelf == true) { // 自己的视角中，只看自己的船（myShips）
            for (Ship<T> s : myShips) {
                if (s.occupiesCoordinates(where)) {
                    return s.getDisplayInfoAt(where, isSelf);
                }
            }
            return null;
        } else {
            return enemyViewBoard.get(where.getRow()).get(where.getColumn());
        }

        // // check whether miss(only check in enemy perspective)
        // if (isSelf == false && enemyMisses.contains(where) == true)
        //     return missInfo;
    }

    @Override
    public Ship<T> fireAt(Coordinate c) {
        // search for a ship that occupies coordinate c
        for (Ship<T> ship : myShips) {
            if (ship.occupiesCoordinates(c) == true) {
                ship.recordHitAt(c);
                enemyViewBoard.get(c.getRow()).set(c.getColumn(), ship.getDisplayInfoAt(c, false));  
                return ship;
            }
        }

        // nothing get hit, record the miss
        enemyMisses.add(c);
        enemyViewBoard.get(c.getRow()).set(c.getColumn(), missInfo);
        return null;
    }

    @Override
    public void recordOldShip(Ship<T> oldShip) {
        oldShips.add(oldShip);
    }

    @Override
    public Ship<T> searchShip(Coordinate c) {
        for (Ship<T> ship : myShips) {
            if (ship.occupiesCoordinates(c) == true)
                return ship;
        }
        return null;
    }

    @Override
    public void deleteNewShip(Ship<T> ship) {
        myShips.remove(ship);
    }

    @Override
    public boolean checkWetherAllSunk() {
        for (Ship<T> ship : myShips) {
            if (ship.isSunk() == false)
                return false;
        }
        return true;
    }

}