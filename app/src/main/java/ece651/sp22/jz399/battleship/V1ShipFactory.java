package ece651.sp22.jz399.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "BattleShip");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    private Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        RectangleShip<Character> ship = new RectangleShip<Character>(name, where.getCoordinate(), where.getOrientation(), w, h, letter, '*');
        return ship;

    }

}