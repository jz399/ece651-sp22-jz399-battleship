package ece651.sp22.jz399.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BoardTextViewTest {
    @Test
    void testDisplayMyOwnBoard() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(30, 10, 'X');
        Board<Character> highBoard = new BattleShipBoard<Character>(10, 30, 'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(highBoard));

        String expected_Body = "As| | A\n" + "B |s| B\n" + "C | |sC\n" + "D | | D\n" + "E | | E\n";
        emptyBoardHelper(3, 5, "  0|1|2\n", expected_Body);
    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expected_Body) {
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0, 0), 'V', 's', '*'));
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 1), 'V', 's', '*'));
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(2, 2), 'V', 's', '*'));

        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expected_Body + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    void testMakeHeader() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
        BoardTextView view = new BoardTextView(b1);
        // System.out.println(view.makeHeader());
        String expected = "  0|1|2|3|4|5|6|7|8|9\n";
        assertEquals(expected, view.makeHeader());
    }

    @Test
    void testDisplayEnemyBoard() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
        V1ShipFactory f = new V1ShipFactory();
        b1.tryAddShip(f.makeDestroyer(new Placement("a0h")));
        b1.fireAt(new Coordinate(0, 0));

        BoardTextView view = new BoardTextView(b1);
        System.out.printf(view.displayEnemyBoard());
        System.out.printf(view.displayMyOwnBoard());
    }

    @Test
    void testDisplayMyBoardWithEnemyNextToIt() {
        V1ShipFactory f = new V1ShipFactory();

        Board<Character> myBoard = new BattleShipBoard<Character>(10, 10, 'X');
        BoardTextView myView = new BoardTextView(myBoard);
        myBoard.tryAddShip(f.makeDestroyer(new Placement("a4h")));
        myBoard.fireAt(new Coordinate(0, 4));

        Board<Character> enemyBoard = new BattleShipBoard<Character>(10, 10, 'X');
        BoardTextView enemyView = new BoardTextView(enemyBoard);
        enemyBoard.tryAddShip(f.makeDestroyer(new Placement("a4h")));
        enemyBoard.fireAt(new Coordinate(0, 4));

        System.out.println(myView.displayMyBoardWithEnemyNextToIt(enemyView, "Your ocean", "Player B's ocean"));

    }
}
