package ece651.sp22.jz399.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
    @Test
    void testMakeCoords() {
        int width = 4;
        int height = 7;
        Coordinate upperleft = new Coordinate(3, 1);
        HashSet<Coordinate> set_V = RectangleShip.makeCoords(upperleft, width, height, 'V');
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                assertTrue(set_V.contains(new Coordinate(upperleft.getRow() + i, upperleft.getColumn() + j)));
            }
        }
        HashSet<Coordinate> set_H = RectangleShip.makeCoords(upperleft, width, height, 'H');
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                assertTrue(set_H.contains(new Coordinate(upperleft.getRow() + j, upperleft.getColumn() + i)));
            }
        }
    }

    @Test
    void testRectangleShip() {
        int width = 4;
        int height = 7;
        Coordinate upperleft = new Coordinate(3, 1);
        RectangleShip<Character> ship = new RectangleShip<Character>("testship", upperleft, 'V', width, height, 's',
                '*');
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                assertTrue(ship.occupiesCoordinates(new Coordinate(upperleft.getRow() + i, upperleft.getColumn() + j)));
            }
        }

    }

    @Test
    void testRectangleShipHit() {
        int width = 3;
        int height = 3;
        Coordinate upperleft = new Coordinate(1, 1);
        RectangleShip<Character> ship = new RectangleShip<Character>("testship", upperleft, 'V', width, height, 's',
                '*');

        // Hit someplaces
        ship.recordHitAt(new Coordinate(upperleft.getRow(), upperleft.getColumn()));
        ship.recordHitAt(new Coordinate(upperleft.getRow() + 1, upperleft.getColumn() + 1));
        ship.recordHitAt(new Coordinate(upperleft.getRow() + 2, upperleft.getColumn() + 2));

        // check hit
        assertTrue(ship.wasHitAt(new Coordinate(upperleft.getRow(), upperleft.getColumn())));
        assertTrue(ship.wasHitAt(new Coordinate(upperleft.getRow() + 1, upperleft.getColumn() + 1)));
        assertTrue(ship.wasHitAt(new Coordinate(upperleft.getRow() + 2, upperleft.getColumn() + 2)));

        // check exception
        assertThrows(IllegalArgumentException.class,
                () -> ship.wasHitAt(new Coordinate(upperleft.getRow() + width, upperleft.getColumn() + height)));
    }

    @Test
    void testIsSunk() {
        int width = 3;
        int height = 3;
        Coordinate upperleft = new Coordinate(1, 1);
        RectangleShip<Character> ship = new RectangleShip<Character>("testship", upperleft, 'V', width, height, 's',
                '*');

        // ship get hits
        for (int i = 0; i < height; i++) {
            ship.recordHitAt(new Coordinate(upperleft.getRow() + i, upperleft.getColumn()));
        }
        assertFalse(ship.isSunk());

        // ship all get hits
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                ship.recordHitAt(new Coordinate(upperleft.getRow() + i, upperleft.getColumn() + j));
            }
        }
        assertTrue(ship.isSunk());
    }

    @Test
    void testgetDisplayInfoAt() {
        int width = 3;
        int height = 3;
        Coordinate upperleft = new Coordinate(1, 1);
        RectangleShip<Character> ship = new RectangleShip<Character>("testship", upperleft, 'V', width, height, 's',
                '*');

        // ship get hits
        for (int i = 0; i < height; i++) {
            ship.recordHitAt(new Coordinate(upperleft.getRow() + i, upperleft.getColumn()));
        }

        // check
        assertEquals('*', ship.getDisplayInfoAt(new Coordinate(upperleft.getRow(), upperleft.getColumn()), true));
        assertEquals('s', ship.getDisplayInfoAt(new Coordinate(upperleft.getRow(), upperleft.getColumn() + 1), true));
    }

    @Test
    void testgetCoordinates() {
        int width = 3;
        int height = 3;
        Coordinate upperleft = new Coordinate(1, 1);
        RectangleShip<Character> ship = new RectangleShip<Character>("testship", upperleft, 'V', width, height, 's',
                '*');

        // check coordinates
        Iterable<Coordinate> set = ship.getCoordinates();
        for (Coordinate c : set) {
            System.out.printf("(row,col) = (%d,%d)\n", c.getRow(), c.getColumn());
        }
    }
}
