package ece651.sp22.jz399.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
    @Test
    void testFactory() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    }

    private void checkShip(Ship<Character> testShip, String expectedName,
            char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[0],true));
        for(Coordinate c:expectedLocs)
        {
            assertTrue(testShip.occupiesCoordinates(c));
        }
    }
}
