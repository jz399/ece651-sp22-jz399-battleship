package ece651.sp22.jz399.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class V2ShipFactoryTest {
    @Test
    void testFactory() {
        V2ShipFactory f = new V2ShipFactory();
        Placement p_v = new Placement(new Coordinate(2,2), 'V');
        Placement p_h = new Placement(new Coordinate(2,2), 'h');
        Placement p_u = new Placement(new Coordinate(2,2), 'U');
        Placement p_d = new Placement(new Coordinate(2,2), 'd');
        Placement p_l = new Placement(new Coordinate(2,2), 'l');
        Placement p_r = new Placement(new Coordinate(2,2), 'R');

        //test false
        f.makeWrongShip_test(p_v);

        //destoryer
        assertThrows(IllegalArgumentException.class, () -> f.makeDestroyer(p_d));
        Ship<Character> dv = f.makeDestroyer(p_v);
        checkShip(dv, "Destroyer", 'd', new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));
        Ship<Character> dh = f.makeDestroyer(p_h);
        checkShip(dh, "Destroyer", 'd', new Coordinate(2, 2), new Coordinate(2, 3), new Coordinate(2, 4));

        //Submarine
        assertThrows(IllegalArgumentException.class, () -> f.makeSubmarine(p_d));
        Ship<Character> sv = f.makeSubmarine(p_v);
        checkShip(sv, "Submarine", 's', new Coordinate(2, 2), new Coordinate(3, 2));
        Ship<Character> sh = f.makeSubmarine(p_h);
        checkShip(sh, "Submarine", 's', new Coordinate(2, 2), new Coordinate(2, 3));

        //Carrier
        assertThrows(IllegalArgumentException.class, () -> f.makeCarrier(p_v));
        Ship<Character> cu = f.makeCarrier(p_u);
        checkShip(cu, "Carrier", 'c', new Coordinate(2, 2), new Coordinate(6, 3));
        Ship<Character> cd = f.makeCarrier(p_d);
        checkShip(cd, "Carrier", 'c', new Coordinate(2, 2), new Coordinate(6, 3));
        Ship<Character> cl = f.makeCarrier(p_l);
        checkShip(cl, "Carrier", 'c', new Coordinate(3, 2), new Coordinate(2, 6));
        Ship<Character> cr = f.makeCarrier(p_r);
        checkShip(cr, "Carrier", 'c', new Coordinate(3, 2), new Coordinate(2, 6));

        //BattleShip
        assertThrows(IllegalArgumentException.class, () -> f.makeBattleship(p_v));
        Ship<Character> bu = f.makeBattleship(p_u);
        checkShip(bu, "BattleShip", 'b', new Coordinate(2, 3), new Coordinate(3, 3));
        Ship<Character> bd = f.makeBattleship(p_d);
        checkShip(bd, "BattleShip", 'b', new Coordinate(2, 3), new Coordinate(3, 3));
        Ship<Character> bl = f.makeBattleship(p_l);
        checkShip(bl, "BattleShip", 'b', new Coordinate(3, 2), new Coordinate(3, 3));
        Ship<Character> br = f.makeBattleship(p_r);
        checkShip(br, "BattleShip", 'b', new Coordinate(3, 3), new Coordinate(3, 3));
    }

    private void checkShip(Ship<Character> testShip, String expectedName,
            char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[0],true));
        for(Coordinate c:expectedLocs)
        {
            assertTrue(testShip.occupiesCoordinates(c));
        }
    }
}
