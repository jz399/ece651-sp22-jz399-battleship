package ece651.sp22.jz399.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PlacementTest {
    @Test
    void testEquals() {
        Placement c1 = new Placement("A1V");
        Placement c2 = new Placement("V2V");
        Placement c3 = new Placement("B4H");
        Placement c4 = new Placement("b4h");
        Placement c5 = new Placement(null,'h');

        c1.toString();

        assertNotEquals(c5,c1);  

        assertEquals(c1, c1); // equals should be reflexsive
        assertEquals(c3, c4); // different objects bu same contents
        assertNotEquals(c1, c3); // different contents
        assertNotEquals(c1, c4);
        assertNotEquals(c3, c2);
        assertNotEquals(c1, "(1, 2)"); // different types
    }

    @Test
    void testHashCode() {
        Placement c1 = new Placement("A1V");
        Placement c2 = new Placement("V2V");
        Placement c3 = new Placement("B4H");
        
        assertNotEquals(c1.hashCode(), c2.hashCode());
        assertNotEquals(c1.hashCode(), c3.hashCode());
        assertNotEquals(c2.hashCode(), c3.hashCode());

        Placement c4 = new Placement(null,'h');
        c4.hashCode();

    }

    @Test
    void testBuildPlacement(){
        assertThrows(IllegalArgumentException.class, () -> new Placement("abcd"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("a1j"));
        assertThrows(IllegalArgumentException.class, () -> new Placement(new Coordinate(1, 2), 'i'));

    }
}
