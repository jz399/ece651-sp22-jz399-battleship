package ece651.sp22.jz399.battleship;


import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
    @Test
    void testCheckMyRule() {
        BattleShipBoard<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
        V1ShipFactory f = new V1ShipFactory();

        Ship<Character> ship_1 = f.makeDestroyer(new Placement(new Coordinate(1, 2), 'V'));
        assertEquals(null,board.tryAddShip(ship_1));

        Ship<Character> ship_2 = f.makeDestroyer(new Placement(new Coordinate(9, 5), 'V'));
        assertEquals("That placement is invalid: the ship goes off the board.\n",board.tryAddShip(ship_2));

        Ship<Character> ship_3 = f.makeDestroyer(new Placement(new Coordinate(5, 9), 'H'));
        assertEquals("That placement is invalid: the ship goes off the board.\n",board.tryAddShip(ship_3));
    }
}
