package ece651.sp22.jz399.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

public class ComputerTextPlayerTest {
    @Test
    void testComputerPlayer() throws IOException{
        String inputData = "k\nN\nN\n";
        PrintStream out = System.out;
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');

        Actions p1_Actions = new Actions(1, 2, out, input);
        Actions p2_Actions = new Actions(1, 2, out, input);

        V2ShipFactory factory = new V2ShipFactory();

        Player p1 = App.selectPlayer(b1, factory, "A", p1_Actions, input, out);
        Player p2 = App.selectPlayer(b2, factory, "B", p2_Actions, input, out);

        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doActionPhase(); // 升级为执行各种动作，不局限于fire
    }
}
